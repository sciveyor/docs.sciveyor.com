---
title: Dataset Management
type: docs
weight: 25
---

In addition to the support for creating datasets [in the analysis
workflow,]({{<ref "workflow.md">}}) there are tools available to manage your
datasets in more detail.

## Creating Datasets

To create datasets, you begin by using the standard [search interface.]({{<ref
"search.md">}}) Use the search and filtering tools until your search results show
the list of articles that you'd like to save for later. Then, click the "Save Results"
button next to the search bar, and give the dataset a name.

While there is a dedicated step for creating datasets in the [analysis
workflow,]({{<ref "workflow.md">}}) you can create datasets by searching at any
time, not only when using the workflow. To access the search interface without
using the workflow, you can either click the "Advanced mode" button on the
dashboard, or you can click "Browse/Search Database" under "Advanced Tools" on
the top menu bar.

## Managing Datasets

To see which datasets you have already created, click "Manage Datasets" under
"Advanced Tools" on the top menu bar. You will then see a list of datasets,
including the number of documents found in each dataset. Datasets can be deleted
from this page as well.

Clicking on the "Manage" button next to a dataset provides more information
about it, including its creation time and size. You can also see a list of all
analyses that have been started using this dataset.
