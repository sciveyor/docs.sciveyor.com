---
title: Analysis Workflow
type: docs
weight: 10
description: >
  How to use the analysis workflow to easily create datasets and analyze them.
---

The primary way to run analyses in Sciveyor is to use the analysis workflow.

## The Dashboard

Once you've [logged in to your account,]({{<ref "manual/Accounts/_index.md">}})
you'll be presented with the dashboard page. You can either start a new
analysis, fetch the results from an analysis you've already run, or enter
advanced mode.

## Starting a New Analysis

### Selecting the Analysis Type

Once you've clicked the button to start a new analysis, you have to choose which
analysis type you'd like to run. You do this by selecting what kind of question
you'd like to ask about the journal literature. Since a single question isn't
sufficient to understand how the analyses work, once you click on one of the
question buttons you will see a detailed description of the analysis to be
performed, including what data is required for the analysis, what results are
returned and the format of those results, and examples of several kinds of
questions that you could use this type of analysis to answer.

Once you have decided which kind of analysis you'd like to run, press the Start
button. This brings you to the data collection page.

### Datasets

Analyses in Sciveyor are run on sets of journal articles that you search for and
save, called _datasets._ Datasets are permanently saved and tied to your user
account. Some types of analysis need one dataset to run, others need more (this
will be detailed in the long description of the analysis).

To create a dataset, click on the "Create another dataset" button below the
(currently empty) list of datasets for your analysis. This will take you to the
search page. For more information on basic searches, search filtering, and
advanced searching, see [the search section of the manual.]({{<ref
"manual/Search/_index.md">}}) Once you have used the search and filtering functions
to narrow down the article database to the articles of interest to you, click on
the green "Save Results" button to the right of the search bar, and give your new
dataset a name. The dataset will be constructed in the background, and when complete,
it will be linked to the analysis currently under construction.

Alternatively, if you already have some datasets you would like to analyze, you
can click the "Link an already created dataset" button and choose them from the
list. If you change your mind about having chosen a dataset, you can remove it
from the list and replace it with another.

### Analysis Options

Once you have selected the correct number of datasets for your analysis, one of
two things will happen. Some jobs have no configurable parameters at all. When
you add the appropriate number of datasets to this type of job, you'll see a
large green button titled "Start Analysis." When you click it, your analysis
will begin.

For most jobs, however, there are at least a few options concerning how you
would like the analysis to run. In that case, once you have selected the
required number of datasets, you'll see a blue button titled "Set Job Options."
When you click it, you'll see a list of options that you can configure which
control how the analysis will be run.

For information about each of the analyses, and the meaning and use of each of
the options that you can configure, see [the analyses section of the
manual.]({{<ref "manual/Analyses/_index.md">}})

Either way, once you have started your analysis, it is placed into a queue to be
performed in the background. You'll be e-mailed when it's finished and it's time
to come collect your data.

## Fetching your Results

If you have a finished job, you can collect the results by clicking on the "See
results of old analyses" button on the dashboard. Finished tasks will either be
available for direct download, or for some tasks, results will be displayed on a
web page. For tasks whose results are displayed on a web page, there will also
be a link to download the task's results for safe-keeping. When your tasks
finish, you should download results soon, as finished analysis data is kept only
for 14 days. Results of _all_ tasks will be available for download in open,
portable formats -- this is one of the core design principles of Sciveyor.

Occasionally, a pending task will either be much too large to complete in a
reasonable time, or will experience some sort of error. If this occurs, you can
choose to cancel all of your pending tasks from the fetch results page.
