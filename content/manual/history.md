---
title: History
type: docs
weight: 6
description: >
  The history of the Sciveyor project.
---

Sciveyor began as two separate projects: RLetters, a Ruby package that was
responsible for the website and analysis code, and evoText, a particular
installation of the RLetters software with a database of articles drawn from
"the evolutionary sciences," broadly speaking. That project began in 2009, and
was initially hosted at the University of Notre Dame, then at Louisiana State
University, and finally followed [Charles Pence](https://charlespence.net) to
the Université catholique de Louvain.

In 2020, we made the decision to combine the RLetters and evoText projects, and
rebrand them as Sciveyor. There were a few reasons for this. First, evoText as a
project was originally limited in scope -- we intentionally made some choices
_not_ to include journal articles that fell outside of the evolutionary
sciences. That made sense at the time (including to our funders), but as we
expanded the corpus, we began to realize (and hear from users!) that the
presence of general-science journals in our corpus, such as _Nature_ and the
catalog from _PLoS,_ meant that evoText was a useful tool for scientists,
philosophers, historians, and others interested not just in the uses of
evolution, but in analyzing any number of sciences. The name "evoText" had thus
become too narrow, and tended to make users underestimate how useful the site
could be.

Second, it was becoming confusing to separate "RLetters," as the software that
powered the website, from evoText itself. That software has also radically
changed, and will soon allow developers to contribute in a variety of languages,
not just Ruby. In that sense, the name of RLetters had also gone out of date.

Since 2020, then, Sciveyor is both a piece of software and a website. If you
would like to build your own corpus of journal articles and analyze them in the
same ways that we do, you can find all of our source code at
<https://codeberg.org/scieveyor>, and you can read more about how to contribute
to our development process here in our developer documentation.
