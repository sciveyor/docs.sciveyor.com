---
title: User Accounts
type: docs
weight: 15
---

Sciveyor requires users to have accounts, so that saved datasets and analysis
results can be associated with the person who owns them.

## Signing Up

To sign up for an account, click the "Sign In" link at the top right of any
page, and click the "Sign up" button. You will have to provide some basic
information, including your name, e-mail address, a password, your preferred
user interface language, and your time zone.

You will only be e-mailed when your analysis jobs complete, and if you request
an e-mail from us to reset a forgotten password. For more information about
privacy, [see our privacy statement.]({{<ref "privacy.md">}})

## Signing In

Once you have an account, click the "Sign In" link at the top right of any page,
fill in your e-mail address and password, and optionally check the box for
Sciveyor to remember your account information. Do not check "Remember me?" on a
public or shared computer.

## Configuring Your Account

Your account has a variety of settings that you can change. To access them,
click "My Account" at the top right of any page. You can change any of the
settings you provided when you signed up, as well as set a few defaults for your
search results.

Sciveyor also has support for looking up full-text journal articles using access
provided by your local library. To take advantage of this feature, visit your My
Account page and scroll to the bottom. You can add your library in two different
ways. By clicking the "Look up your library automatically" button, the
[WorldCat](https://www.worldcat.org/) library database is consulted to provide a
list of your closest academic libraries. If your library is available in the
list, click on the appropriate button to add it to your user account.
Alternatively, you can add your library manually. To do so, look for your
library in the [WorldCat Registry](http://worldcat.org/registry/Institutions),
or ask your local librarian.

Once you've added your local library, to look up an article in your library,
click the "More" link next to the article. In the list of links that appears,
under the "Download from library" header, you'll see a link that looks like
"Your library:" followed by the name of the library that you added. Click it,
and you'll visit a page for the article, provided through your library's access.

## Canceling Your Account

Your account can be canceled at any time from the "My Account" page. Click the
"My Account" link at the top right of any page, and scroll down to the red
button labeled "Cancel my account."
