---
title: Tutorial
weight: 3
type: docs
description: >
  A more detailed tutorial for running your first analysis in Sciveyor.
---

This follows the structure of the quick start above, but with more information
about each step.

1.  **Sign up for a new user account, and log in.**

    It's important to create a user account, so that your personal, custom
    datasets and analysis results can be saved and associated with your e-mail
    address. Further, when a long-running analysis job finishes, you will be
    e-mailed so that you know to return to recover your data.

    We take privacy particularly seriously -- check out the [privacy
    policy]({{<ref "privacy.md">}}) for more information. You won't be spammed,
    and you can delete your account at any time.

2.  **From your dashboard page, click the button to start a new analysis.**

    The dashboard is your central focus for analysis work. From there, you can
    perform three tasks -- start a new analysis, fetch results from your old
    analyses, and enter advanced mode. (See the [detailed user manual]({{<ref
    "workflow.md">}}) for information about advanced mode.)

3.  **Select the question, "When were a given set of articles published?"**

    After the new analysis button is pressed, you are presented with a list of
    questions. Each of these questions corresponds to a variety of analysis that
    you can run on the database of journal articles. Each of these analyses also
    comes with detailed explanation (see the next step). Beyond that, the best
    bet is to experiment!

4.  **Read the information about the analysis, and click Start.**

    Here you see a detailed page of information about the analysis that will be
    run -- including what kind of data is returned, the formats in which it is
    available, and what kinds of research questions it could be used to answer.

5.  **Click the button to create another dataset.**

    Once you have chosen the analysis you would like to run, you now have to
    choose which articles you'd like to apply it to. You do this by collecting
    articles into named groups called datasets.

6.  **Click "Save Results" next to the search bar.**

    To create a dataset, you visit the search page, and filter and search the
    article database until you're left with the set of articles you'd like to
    analyze. Then, click "Save Results" to permanently save this set of articles
    as a dataset. That dataset will then be linked with the analysis that you're
    currently preparing to run.

7.  **Give this dataset a name, and click "Create Dataset."**

    Your datasets will persist permanently, and you can use the same dataset in
    multiple analyses over time.

8.  **Click "Set Job Options."**

    Once you have provided enough data to run the analysis (sometimes one
    dataset, sometimes more), you will then be asked to set detailed options for
    how the analysis should be run. These options vary depending on the
    analysis. For more information, check out the detailed manual, which
    describes all of the possible options for all of the varieties of analysis.

9.  **Click "Start Analysis Job."**

    With the options set, you can then start your analysis job. Analyses are run
    in the background, and you will be e-mailed when the analysis completes.

10. **After waiting for the job to finish, visit the dashboard and click the
    button to see the results of your old analyses.**

    Most analysis jobs either show their results as a web page (with a download
    link provided), or are directly offered as a file download. All data will be
    available in free and open file formats.

11. **In the list, click "View" next to your newly finished job.**

    In this case, the analysis you chose for the tutorial shows its results as a
    web page. You'll also see a "Delete" link, which you can use once you've
    saved your data. Note that analysis results are kept for only fourteen days
    after the analysis completes, so make sure to download a copy of the data to
    keep.

12. **You should see a graph showing the publication dates of all of the
    articles available in the database.**

    When results are available as a web page, you'll see a download button in
    the top right, which lets you save a copy of your results. You'll also see
    various visualizations of your data, including graphs and sortable tables.

With that, you should be able to create datasets and analyze them. For more
information about the available analyses and their options, read the detailed
manual.
