---
title: "User Manual"
linkTitle: "User Manual"
type: docs
description: Welcome to the Sciveyor user manual!
menu:
  main:
    weight: 10
---

Sciveyor is a site designed to allow you perform complex searches and textual
analyses on a corpus of scientific journal articles. In short, Sciveyor allows
you to run a search to find the articles that you're interested in, save the
results of that search as a "dataset" (producing a permanent record that you can
return to later), and then analyze those datasets in a variety of ways.

If you need help running an analysis or searching the database, this is where to
look. You'll find basic information here about the kinds of analysis jobs that
you can perform and some frequently asked questions. Check out all of the
content available in the menu on the left-hand side.

If you're interested in technical documentation, such as our support for various
web and library standards, check out our [developer
documentation.]({{<ref "/content/docs/_index.md">}})
