---
title: Article Dates
type: docs
description: Plot number of articles in a dataset by date of publication.
---

To start this job, select the question: "When were a given set of articles
published?"

This job results in a graph of the publication dates of your dataset, as well as
the counts downloadable as a CSV file. When the graph is computed, you may
optionally choose to divide the absolute counts of articles published in a given
year by the counts in another dataset or in the entire corpus, giving you the
_fraction_ of articles in the larger set which appear in your search (see the
options section below for more information). By simply counting the publication
frequency over time, you can actually answer a wide variety of questions:

> How much of the literature in a given time period was focused on a given
> topic? _(Input: a dataset created from a search for this topic, normalizing
> against the entire corpus)_

## Options

When you run this analysis, by default it will simply return the number of
articles published in each year in your dataset. Often, however, you would like
to know these counts as _fractions_ -- for instance, how many of the articles
published in each year in the entire corpus are found in this dataset? We call
this _normalizing_ the counts.

To normalize, check the box and choose the dataset (or the entire corpus) that
you'd like to normalize by. This will take the counts of articles per year in
this dataset and divide them by the counts of articles per year in the dataset
you select.
