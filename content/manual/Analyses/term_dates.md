---
title: Term Dates
type: docs
description: Plot the occurrences of a term in a dataset by date.
---

To start this job, select the questions: "How has the frequency of a term
changed over time? When was a word used within a particular dataset?"

This job results in a graph of the occurrences of the given term within your
dataset, plotted by date, as well as those occurrences downloadable as a CSV
file. This allows us to answer a wide variety of questions:

> How has the frequency of use of a term changed over time? _(Input: a dataset
> of interest, plotting for the use of a given term)_
>
> When was a term first introduced into the literature? _(Input: a dataset of
> interest, looking for the place when the term is first introduced)_
>
> How has a term moved through the literature? _(Input: comparing these graphs
> for the same term across different journals and time periods)_

## Options

The only option for this analysis is to select the term that you would like to
search for.
