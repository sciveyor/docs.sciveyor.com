---
title: Proper Names
type: docs
description: Extract all instances of proper names.
---

This task will read through a set of texts and extract any references that it
can find to proper names, such as those of persons, organizations, and places.
The accuracy of the proper name extractor is lower than might be desired, but
this job can provide an interesting way to begin exploring the following kinds
of questions:

> What organizations are commonly correlated with certain kinds of research?
>
> What locations do researchers in a given field usually work or live in?
>
> Have regions of foucus in a body of literature changed over time?

## Options

This analysis has no options.
