---
title: Types of Analysis
linkTitle: Analyses
type: docs
weight: 100
description: >
  Descriptions of the various kinds of analyses that can be performed on the
  datasets that you create in Sciveyor.
---

For more information about the analysis tasks that you can run using Sciveyor,
visit the following pages:
