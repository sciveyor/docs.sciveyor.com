---
title: API Reference
linkTitle: API Reference
weight: 400
type: swagger
description: >
  A reference for all publicly supported API calls to the Sciveyor server.
---

{{< swaggerui src="/api-schema/sciveyor-v1.yaml" >}}
