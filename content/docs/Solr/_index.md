---
title: SOLR Reference
linkTitle: SOLR Reference
weight: 700
description: >
  Documentation about the Sciveyor Solr server, its schema, and the kinds of
  searches that it supports.
---

The central server used by Sciveyor as the backend both for searching and for
retrieving text content for analysis is stored in
[Apache Solr.](https://solr.apache.org/) These pages contain information for
developers who are interested in interacting with that Solr server via its API.

**N.B.:** This Solr server is _not_ available for public access; it may only be
used by developers interested in writing plugins for Sciveyor that run inside
our datacenter.
