---
title: SQL Schema Reference
linkTitle: SQL Schema Reference
weight: 900
description: >
  A description of the schema for Sciveyor's SQL database, with which worker
  applications interface.
---

Worker applications that are not using one of the already implemented [worker
API packages]({{<relref "docs/Workers/_index.md">}}) will need to interface
directly with the Sciveyor PostgreSQL database. Information about the schema of
that database can be [read online here.](/schemaspy/)
