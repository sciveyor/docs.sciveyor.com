---
title: Configuration
linkTitle: Configuration
weight: 2
description: >
  Configuration variables available for the Sciveyor client.
---

The configuration for the Rails app is passed in as environment variables,
following the [Twelve-Factor App](https://12factor.net/config) idea of a
complete separation between configuration and code.

These can be injected using environment files (for example with our Docker
containers), or via a configuration/secrets management service. The Sciveyor
development team itself uses (and recommends!)
[Doppler.](https://www.doppler.com)

The following settings can be configured using environment variables. Default
values are shown, when they exist.

```sh
# ----------------------------------------------------------------------------
# Server settings

# The final URL at which we're deployed. The default configuration, in
# development, would yield "http://localhost:3000/".
APP_URL=

# Puma can serve the application code in multiple worker processes if you
# configure it here.
RAILS_WEB_WORKERS=2

# Each worker process can then run the requested number of Rails threads.
RAILS_MAX_THREADS=5

# If this variable is set to true, force all app connections to occur
# over HTTPS, and send secure-only cookies.
HTTPS_ONLY=false

# If this variable is set to true, jobs will not actually delay, and will run
# immediately, blocking the web server. Useful only for local deployments on
# small datasets or for testing.
BLOCKING_JOBS=false

# The maximum amount of time that a job can be run before it will be killed.
# Set this timeout to whatever you find reasonable for your installation, in
# hours. Twelve hours is a reasonable default; jobs running longer than that
# may well have failed, unless you have a very large corpus.
JOB_TIMEOUT=12

# URL to the database. We only support PostgreSQL, and we recommend a pool
# size of at least 20, to account for concurrency in the frontend and a
# reasonable number of workers in the backend.
#
# A reasonable default is "postgres:///sciveyor", which will connect to the
# local machine's Postgres socket and look for the "sciveyor" database.
DATABASE_URL=

# URL to the Solr instance. If you are running Solr using our example Docker
# configuration (https://codeberg.org/sciveyor/solr-docker), the URL should
# be "http://127.0.0.1:8983/solr/sciveyor".
SOLR_URL=

# The timeout for connections to Solr, in seconds. The default is incredibly
# high, because running large Solr queries can result in exceptional query
# times in many configurations. Lower this at your own risk.
SOLR_TIMEOUT=120

# S3 credentials for file storage. You *must* set these variables in
# production, because otherwise there will be no way for Sciveyor's web and
# job-worker processes to share files. Development and testing will write to
# the local filesystem.
S3_ENDPOINT=s3.amazonaws.com
S3_ACCESS_KEY_ID=
S3_SECRET_ACCESS_KEY=
S3_BUCKET=bucket_name
S3_REGION=us-east-1


# ----------------------------------------------------------------------------
# Email settings

# Configuration variables for mail delivery
SMTP_ADDRESS=localhost
SMTP_PORT=25
SMTP_DOMAIN= # a HELO domain, if required
SMTP_USERNAME=
SMTP_PASSWORD=
SMTP_AUTHENTICATION= # can be: plain,login,cram_md5
SMTP_ENABLE_STARTTLS_AUTO=true
SMTP_SSL=false
SMTP_OPENSSL_VERIFY_MODE= # can be: none,peer


# ----------------------------------------------------------------------------
# Feature flags

# If specified, show a maintenance message at the top of every page as a
# non-dismissable alert.
MAINTENANCE_MESSAGE=

# Every analysis job can be optionally disabled using the feature flags in
# the following block, if your users do not require them and/or they are
# proving computationally prohibitive on your installation.
ARTICLE_DATES_JOB_DISABLED=false
COLLOCATION_JOB_DISABLED=false
COOCCURRENCE_JOB_DISABLED=false
CRAIG_ZETA_JOB_DISABLED=false
EXPORT_CITATIONS_JOB_DISABLED=false
PROPER_NAMES_JOB_DISABLED=false
NETWORK_JOB_DISABLED=false
TERM_DATES_JOB_DISABLED=false
# Note: disabling the UserExport job will violate the GDPR.
USER_EXPORT_JOB_DISABLED=false
WORD_FREQUENCY_JOB_DISABLED=false


# ----------------------------------------------------------------------------
# Secret keys

# These two variables must be set before you start up in production, or an
# error will be raised. Generate with `bin/rails secret`. Development and
# testing will generate temporary keys.
SECRET_KEY_BASE=
DEVISE_SECRET_KEY=
```
