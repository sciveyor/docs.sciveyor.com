---
title: Worker Reference
linkTitle: Worker Reference
weight: 600
description: >
  A reference for anyone interested in building workers, i.e., code that can
  perform analysis tasks that run on the Sciveyor platform.
---
