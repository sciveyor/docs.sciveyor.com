---
title: Security Policy
weight: 11
description: >
  Information about how to disclose security vulnerabilities and what we pledge
  to do in response.
---

We work very hard to ensure the security of our systems, especially as regards the disclosure of user information and copyrighted journal article material. But no matter how much effort we put into system security, there can still be vulnerabilities present.

If you discover a vulnerability, we would like to know about it so we can take steps to address it as quickly as possible. We would like to ask you to help us better protect our users and our systems.

Please do the following:

- E-mail your findings to <charles@charlespence.net>. Encrypt your findings using [my PGP key](https://keyoxide.org/F371AEEAF417E9490447C9C61F6099E4BD9DEC90) to prevent this critical information from falling into the wrong hands.
- Do not take advantage of the vulnerability or problem you have discovered, for example by downloading more data than necessary to demonstrate the vulnerability or deleting or modifying other people's data.
- Do not reveal the problem to others until it has been resolved.
- Do not use attacks on physical security, social engineering, distributed denial of service, spam or applications of third parties.
- Do provide sufficient information to reproduce the problem, so we will be able to resolve it as quickly as possible. Usually, the IP address or the URL of the affected system and a description of the vulnerability will be sufficient, but complex vulnerabilities may require further explanation.

What we promise:

- We will respond to your report within 14 days, with our evaluation of the report and an expected resolution date.
- If you have followed the instructions above, we will not take any legal action against you in regard to the report.
- We will handle your report with strict confidentiality, and not pass on your personal details to third parties without your permission.
- We will keep you informed of the progress towards resolving the problem.
- In the public information concerning the problem reported, we will give your name as the discoverer of the problem (unless you desire otherwise), and gratefully mention you on our [thanks page.]({{< relref "thanks.md" >}})
- As a token of our gratitude for your assistance, we offer a reward for every report of a security problem that was not yet known to us. The amount of the reward will be determined based on the severity of the leak and the quality of the report. The minimum reward will be a €20 gift certificate (I'm just a poor academic, after all).

We strive to resolve all problems as quickly as possible, and we would like to play an active role in the ultimate publication of the problem after it is resolved.

_[adapted and modified from the version prepared by Floor Terra, found here under CC-BY 3.0](https://responsibledisclosure.nl/en/)_
